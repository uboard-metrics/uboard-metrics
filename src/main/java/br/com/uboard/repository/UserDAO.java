package br.com.uboard.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.com.uboard.model.User;

@Repository
public interface UserDAO extends JpaRepository<User, Long> {

	Optional<User> findByUsername(String username);

	Optional<User> findByUboardIdentifier(String uboardIdentifier);

	Optional<User> findByEmail(String email);
}
