package br.com.uboard.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.com.uboard.model.Developer;
import br.com.uboard.model.Metric;

@Repository
public interface DeveloperDAO extends JpaRepository<Developer, Long> {

	List<Developer> findByMetricAndNameContains(Metric metric, String name);
}
