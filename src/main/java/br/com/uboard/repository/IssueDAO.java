package br.com.uboard.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.com.uboard.model.Issue;
import br.com.uboard.model.Metric;

@Repository
public interface IssueDAO extends JpaRepository<Issue, Long> {

	List<Issue> findByMetricAndTitleContains(Metric metric, String title);
}
