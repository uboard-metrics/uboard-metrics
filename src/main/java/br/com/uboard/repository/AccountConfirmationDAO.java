package br.com.uboard.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.com.uboard.model.AccountConfirmation;
import br.com.uboard.model.User;

@Repository
public interface AccountConfirmationDAO extends JpaRepository<AccountConfirmation, Long> {

	public Optional<AccountConfirmation> findByCodeAndUserEmail(String code, String email);
	
	public Optional<AccountConfirmation> findByCodeAndUserEmailAndType(String code, String email, String type);

	public Optional<AccountConfirmation> findByUser(User user);

	public Optional<AccountConfirmation> findByUserAndType(User user, String type);
}
