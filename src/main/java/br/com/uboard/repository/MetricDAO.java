package br.com.uboard.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.com.uboard.model.Metric;
import br.com.uboard.model.User;

@Repository
public interface MetricDAO extends JpaRepository<Metric, Long> {

	List<Metric> findByUserAndMilestoneContains(User user, String milestone);

	Optional<Metric> findByMilestoneAndUser(String milestone, User user);

	Optional<Metric> findByUserAndUboardIdentifier(User user, String uboardIdentifier);
}
