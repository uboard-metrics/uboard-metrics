package br.com.uboard.exception;

public class InvalidConfirmationCodeException extends Exception {

	private static final long serialVersionUID = 1L;

	public InvalidConfirmationCodeException() {

	}

	public InvalidConfirmationCodeException(String message) {
		super(message);
	}

	public InvalidConfirmationCodeException(String message, Exception e) {
		super(message, e);
	}
}
