package br.com.uboard.exception;

public class UserAlreadyActivatedException extends Exception {

	private static final long serialVersionUID = 1L;

	public UserAlreadyActivatedException() {

	}

	public UserAlreadyActivatedException(String message) {
		super(message);
	}

	public UserAlreadyActivatedException(String message, Exception e) {
		super(message, e);
	}
}
