package br.com.uboard.model.enums;

public enum AccountConfirmationTypeEnum {

	ACTIVATE_USER("ACTIVATE_USER"), RESET_PASSWORD("RESET_PASSWORD");

	private String name;

	private AccountConfirmationTypeEnum(String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}
}
