package br.com.uboard.model;

import java.util.Date;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

import br.com.uboard.model.transport.MetricDTO;
import jakarta.persistence.CascadeType;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.OneToMany;
import jakarta.persistence.OneToOne;

@Entity
public class Metric {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@Column(nullable = false, unique = true)
	private String uboardIdentifier;

	@Column(nullable = false)
	private String milestone;

	@Column(nullable = false)
	private Date startDate;

	@Column(nullable = true)
	private Date dueDate;

	@Column(nullable = false)
	private Integer totalIssues;

	@Column(nullable = false)
	private Integer openIssuesCount;

	@Column(nullable = false)
	private Integer closedIssuesCount;

	@Column(nullable = false)
	private Integer reopenedIssuesCount;

	@Column(nullable = false)
	private Double conclusionPercent;

	@Column(nullable = false)
	private Double deficit;

	@OneToOne
	@JoinColumn(name = "user_id", referencedColumnName = "id", nullable = false)
	private User user;

	@OneToMany(mappedBy = "metric", cascade = CascadeType.ALL)
	private List<Developer> developers;

	@OneToMany(mappedBy = "metric", cascade = CascadeType.ALL)
	private List<Issue> issues;

	public Metric() {

	}

	public Metric(MetricDTO metricDTO) {
		this.uboardIdentifier = UUID.randomUUID().toString();
		this.milestone = metricDTO.getMilestone();
		this.startDate = metricDTO.getStartDate();
		this.dueDate = metricDTO.getDueDate() != null ? metricDTO.getDueDate() : null;
		this.totalIssues = metricDTO.getTotalIssues();
		this.openIssuesCount = metricDTO.getOpenIssuesCount();
		this.closedIssuesCount = metricDTO.getClosedIssuesCount();
		this.reopenedIssuesCount = metricDTO.getReopenedIssuesCount();
		this.conclusionPercent = metricDTO.getConclusionPercent();
		this.deficit = metricDTO.getDeficit();

		if (metricDTO.getDevelopers() != null && !metricDTO.getDevelopers().isEmpty()) {
			this.developers = metricDTO.getDevelopers().stream().map(Developer::new).collect(Collectors.toList());
			if (this.developers != null && !this.developers.isEmpty()) {
				this.developers.forEach(developer -> developer.setMetric(this));
			}
		}

		if (metricDTO.getIssues() != null && !metricDTO.getIssues().isEmpty()) {
			this.issues = metricDTO.getIssues().stream().map(Issue::new).collect(Collectors.toList());
			if (this.issues != null && !this.issues.isEmpty()) {
				this.issues.forEach(issue -> issue.setMetric(this));
			}
		}

		if (metricDTO.getOwner() != null) {
			this.user = new User();
			this.user.setUboardIdentifier(metricDTO.getOwner().getUboardIdentifier());
		}
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getUboardIdentifier() {
		return uboardIdentifier;
	}

	public void setUboardIdentifier(String uboardIdentifier) {
		this.uboardIdentifier = uboardIdentifier;
	}

	public String getMilestone() {
		return milestone;
	}

	public void setMilestone(String milestone) {
		this.milestone = milestone;
	}

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public Date getDueDate() {
		return dueDate;
	}

	public void setDueDate(Date dueDate) {
		this.dueDate = dueDate;
	}

	public Integer getTotalIssues() {
		return totalIssues;
	}

	public void setTotalIssues(Integer totalIssues) {
		this.totalIssues = totalIssues;
	}

	public Integer getOpenIssuesCount() {
		return openIssuesCount;
	}

	public void setOpenIssuesCount(Integer openIssuesCount) {
		this.openIssuesCount = openIssuesCount;
	}

	public Integer getClosedIssuesCount() {
		return closedIssuesCount;
	}

	public void setClosedIssuesCount(Integer closedIssuesCount) {
		this.closedIssuesCount = closedIssuesCount;
	}

	public Integer getReopenedIssuesCount() {
		return reopenedIssuesCount;
	}

	public void setReopenedIssuesCount(Integer reopenedIssuesCount) {
		this.reopenedIssuesCount = reopenedIssuesCount;
	}

	public Double getConclusionPercent() {
		return conclusionPercent;
	}

	public void setConclusionPercent(Double conclusionPercent) {
		this.conclusionPercent = conclusionPercent;
	}

	public Double getDeficit() {
		return deficit;
	}

	public void setDeficit(Double deficit) {
		this.deficit = deficit;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public List<Developer> getDevelopers() {
		return developers;
	}

	public void setDevelopers(List<Developer> developers) {
		this.developers = developers;
	}

	public List<Issue> getIssues() {
		return issues;
	}

	public void setIssues(List<Issue> issues) {
		this.issues = issues;
	}

}
