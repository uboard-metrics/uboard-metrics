package br.com.uboard.model.transport;

import java.io.Serializable;

import br.com.uboard.model.transport.interfaces.AccountConfirmationInterface;
import jakarta.validation.constraints.NotBlank;

public class AccountConfirmationDTO implements Serializable, AccountConfirmationInterface {

	private static final long serialVersionUID = 1L;

	@NotBlank(message = "Activate account code must be provided")
	private String code;

	@NotBlank(message = "Account email must be provided")
	private String email;

	@Override
	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	@Override
	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

}
