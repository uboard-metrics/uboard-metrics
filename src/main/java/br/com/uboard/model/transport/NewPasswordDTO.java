package br.com.uboard.model.transport;

import java.io.Serializable;

import br.com.uboard.model.transport.interfaces.AccountConfirmationInterface;
import jakarta.validation.constraints.NotBlank;

public class NewPasswordDTO implements Serializable, AccountConfirmationInterface {

	private static final long serialVersionUID = 1L;

	@NotBlank(message = "Confirmation account code must be provided")
	private String code;

	@NotBlank(message = "Account email must be provided")
	private String email;

	@NotBlank(message = "Account password must be provided")
	private String password;

	@Override
	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	@Override
	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

}
