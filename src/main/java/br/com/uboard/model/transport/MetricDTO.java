package br.com.uboard.model.transport;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import br.com.uboard.model.Metric;

public class MetricDTO implements Serializable {

	private static final long serialVersionUID = 1L;
	private String uboardIdentifier;
	private String milestone;
	private Date startDate;
	private Date dueDate;
	private Integer totalIssues;
	private Integer openIssuesCount;
	private Integer closedIssuesCount;
	private Integer reopenedIssuesCount;
	private Double conclusionPercent;
	private Double deficit;
	private UserDTO owner;
	private List<DeveloperDTO> developers;
	private List<IssueDTO> issues;

	public MetricDTO() {

	}

	public MetricDTO(Metric metric) {
		this.uboardIdentifier = metric.getUboardIdentifier();
		this.milestone = metric.getMilestone();
		this.startDate = metric.getStartDate();
		this.dueDate = metric.getDueDate() != null ? metric.getDueDate() : null;
		this.totalIssues = metric.getTotalIssues();
		this.openIssuesCount = metric.getOpenIssuesCount();
		this.closedIssuesCount = metric.getClosedIssuesCount();
		this.reopenedIssuesCount = metric.getReopenedIssuesCount();
		this.conclusionPercent = metric.getConclusionPercent();
		this.deficit = metric.getDeficit();
		this.owner = new UserDTO(metric.getUser());
		this.developers = new ArrayList<>();
		this.issues = new ArrayList<>();
	}

	public String getUboardIdentifier() {
		return uboardIdentifier;
	}

	public void setUboardIdentifier(String uboardIdentifier) {
		this.uboardIdentifier = uboardIdentifier;
	}

	public String getMilestone() {
		return milestone;
	}

	public void setMilestone(String milestone) {
		this.milestone = milestone;
	}

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public Date getDueDate() {
		return dueDate;
	}

	public void setDueDate(Date dueDate) {
		this.dueDate = dueDate;
	}

	public Integer getTotalIssues() {
		return totalIssues;
	}

	public void setTotalIssues(Integer totalIssues) {
		this.totalIssues = totalIssues;
	}

	public Integer getOpenIssuesCount() {
		return openIssuesCount;
	}

	public void setOpenIssuesCount(Integer openIssuesCount) {
		this.openIssuesCount = openIssuesCount;
	}

	public Integer getClosedIssuesCount() {
		return closedIssuesCount;
	}

	public void setClosedIssuesCount(Integer closedIssuesCount) {
		this.closedIssuesCount = closedIssuesCount;
	}

	public Integer getReopenedIssuesCount() {
		return reopenedIssuesCount;
	}

	public void setReopenedIssuesCount(Integer reopenedIssuesCount) {
		this.reopenedIssuesCount = reopenedIssuesCount;
	}

	public Double getConclusionPercent() {
		return conclusionPercent;
	}

	public void setConclusionPercent(Double conclusionPercent) {
		this.conclusionPercent = conclusionPercent;
	}

	public Double getDeficit() {
		return deficit;
	}

	public void setDeficit(Double deficit) {
		this.deficit = deficit;
	}

	public UserDTO getOwner() {
		return owner;
	}

	public void setOwner(UserDTO owner) {
		this.owner = owner;
	}

	public List<DeveloperDTO> getDevelopers() {
		return developers;
	}

	public void setDevelopers(List<DeveloperDTO> developers) {
		this.developers = developers;
	}

	public List<IssueDTO> getIssues() {
		return issues;
	}

	public void setIssues(List<IssueDTO> issues) {
		this.issues = issues;
	}

}
