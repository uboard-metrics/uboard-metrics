package br.com.uboard.model.transport.interfaces;

public interface AccountConfirmationInterface {

	public String getCode();

	public String getEmail();
}
