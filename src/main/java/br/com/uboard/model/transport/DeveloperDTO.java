package br.com.uboard.model.transport;

import java.io.Serializable;

import br.com.uboard.model.Developer;

public class DeveloperDTO implements Serializable {

	private static final long serialVersionUID = 1L;
	private String uboardIdentifier;
	private String name;
	private Integer completedIssues;
	private Double conclusionPercent;
	private Boolean isIdle;

	public DeveloperDTO() {

	}

	public DeveloperDTO(Developer developer) {
		this.uboardIdentifier = developer.getUboardIdentifier();
		this.name = developer.getName();
		this.completedIssues = developer.getCompletedIssues();
		this.conclusionPercent = developer.getConclusionPercent();
		this.isIdle = developer.isIdle();
	}

	public String getUboardIdentifier() {
		return uboardIdentifier;
	}

	public void setUboardIdentifier(String uboardIdentifier) {
		this.uboardIdentifier = uboardIdentifier;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Integer getCompletedIssues() {
		return completedIssues;
	}

	public void setCompletedIssues(Integer completedIssues) {
		this.completedIssues = completedIssues;
	}

	public Double getConclusionPercent() {
		return conclusionPercent;
	}

	public void setConclusionPercent(Double conclusionPercent) {
		this.conclusionPercent = conclusionPercent;
	}

	public Boolean getIsIdle() {
		return isIdle;
	}

	public void setIsIdle(Boolean isIdle) {
		this.isIdle = isIdle;
	}

}
