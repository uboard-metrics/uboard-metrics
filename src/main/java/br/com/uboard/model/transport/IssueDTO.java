package br.com.uboard.model.transport;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.UUID;

import br.com.uboard.model.Issue;

public class IssueDTO implements Serializable {

	private static final long serialVersionUID = 1L;

	private Long id;
	private String uboardIdentifier;
	private String title;
	private Date createdAt;
	private Date closedAt;
	private String state;
	private Boolean hasAssignees;
	private List<Long> assignees;

	public IssueDTO() {

	}

	public IssueDTO(Issue issue) {
		this.uboardIdentifier = UUID.randomUUID().toString();
		this.title = issue.getTitle();
		this.createdAt = issue.getCreatedAt();
		this.closedAt = issue.getClosedAt() != null ? issue.getClosedAt() : null;
		this.state = issue.getState();
		this.hasAssignees = issue.isHasAssignees();
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getUboardIdentifier() {
		return uboardIdentifier;
	}

	public void setUboardIdentifier(String uboardIdentifier) {
		this.uboardIdentifier = uboardIdentifier;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public Date getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
	}

	public Date getClosedAt() {
		return closedAt;
	}

	public void setClosedAt(Date closedAt) {
		this.closedAt = closedAt;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public Boolean getHasAssignees() {
		return hasAssignees;
	}

	public void setHasAssignees(Boolean hasAssignees) {
		this.hasAssignees = hasAssignees;
	}

	public List<Long> getAssignees() {
		return assignees;
	}

	public void setAssignees(List<Long> assignees) {
		this.assignees = assignees;
	}
}
