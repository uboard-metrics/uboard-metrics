package br.com.uboard.model;

import java.util.UUID;

import br.com.uboard.model.transport.DeveloperDTO;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;

@Entity
public class Developer {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@Column(nullable = false, unique = true)
	private String uboardIdentifier;

	@Column(nullable = false)
	private String name;

	@Column(nullable = false)
	private Integer completedIssues;

	@Column(nullable = false)
	private Double conclusionPercent;

	@Column(columnDefinition = "tinyint DEFAULT 0")
	private boolean isIdle;

	@ManyToOne
	@JoinColumn(name = "metric_id", nullable = false)
	private Metric metric;

	public Developer() {

	}

	public Developer(DeveloperDTO developerDTO) {
		this.uboardIdentifier = UUID.randomUUID().toString();
		this.name = developerDTO.getName();
		this.completedIssues = developerDTO.getCompletedIssues();
		this.conclusionPercent = developerDTO.getConclusionPercent();
		this.isIdle = developerDTO.getIsIdle() != null ? developerDTO.getIsIdle() : false;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getUboardIdentifier() {
		return uboardIdentifier;
	}

	public void setUboardIdentifier(String uboardIdentifier) {
		this.uboardIdentifier = uboardIdentifier;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Integer getCompletedIssues() {
		return completedIssues;
	}

	public void setCompletedIssues(Integer completedIssues) {
		this.completedIssues = completedIssues;
	}

	public Double getConclusionPercent() {
		return conclusionPercent;
	}

	public void setConclusionPercent(Double conclusionPercent) {
		this.conclusionPercent = conclusionPercent;
	}

	public boolean isIdle() {
		return isIdle;
	}

	public void setIdle(boolean isIdle) {
		this.isIdle = isIdle;
	}

	public Metric getMetric() {
		return metric;
	}

	public void setMetric(Metric metric) {
		this.metric = metric;
	}

}
