package br.com.uboard.model;

import java.util.Date;
import java.util.UUID;

import br.com.uboard.model.transport.IssueDTO;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;

@Entity
public class Issue {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@Column(nullable = false, unique = true)
	private String uboardIdentifier;

	@Column(nullable = false)
	private String title;

	@Column(nullable = false)
	private Date createdAt;

	@Column(nullable = true)
	private Date closedAt;

	@Column(nullable = false)
	private String state;

	@Column(columnDefinition = "tinyint DEFAULT 0")
	private boolean hasAssignees;

	@ManyToOne
	@JoinColumn(name = "metric_id", nullable = false)
	private Metric metric;

	public Issue() {

	}

	public Issue(IssueDTO issueDTO) {
		this.uboardIdentifier = UUID.randomUUID().toString();
		this.title = issueDTO.getTitle();
		this.createdAt = issueDTO.getCreatedAt();
		this.closedAt = issueDTO.getClosedAt() != null ? issueDTO.getClosedAt() : null;
		this.state = issueDTO.getState();
		this.hasAssignees = issueDTO.getHasAssignees() != null ? issueDTO.getHasAssignees() : false;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getUboardIdentifier() {
		return uboardIdentifier;
	}

	public void setUboardIdentifier(String uboardIdentifier) {
		this.uboardIdentifier = uboardIdentifier;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public Date getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
	}

	public Date getClosedAt() {
		return closedAt;
	}

	public void setClosedAt(Date closedAt) {
		this.closedAt = closedAt;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public boolean isHasAssignees() {
		return hasAssignees;
	}

	public void setHasAssignees(boolean hasAssignees) {
		this.hasAssignees = hasAssignees;
	}

	public Metric getMetric() {
		return metric;
	}

	public void setMetric(Metric metric) {
		this.metric = metric;
	}

}
