package br.com.uboard.service;

import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import br.com.uboard.config.kafka.producer.Topics;
import br.com.uboard.exception.InvalidConfirmationCodeException;
import br.com.uboard.model.AccountConfirmation;
import br.com.uboard.model.User;
import br.com.uboard.model.transport.CredentialsDTO;
import br.com.uboard.model.transport.interfaces.AccountConfirmationInterface;
import br.com.uboard.repository.AccountConfirmationDAO;
import br.com.uboard.repository.UserDAO;

@Service
public class AccountConfirmationService {

	private AccountConfirmationDAO accountConfirmationDAO;

	private UserDAO userDAO;

	private KafkaTemplate<String, List<CredentialsDTO>> kafkaTemplate;

	public AccountConfirmationService(AccountConfirmationDAO accountConfirmationDAO, UserDAO userDAO,
			KafkaTemplate<String, List<CredentialsDTO>> kafkaTemplate) {
		this.accountConfirmationDAO = accountConfirmationDAO;
		this.userDAO = userDAO;
		this.kafkaTemplate = kafkaTemplate;
	}

	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public void activate(AccountConfirmationInterface accountConfirmationInterface) throws Exception {

		AccountConfirmation accountConfirmation = this.validate(accountConfirmationInterface);
		User user = accountConfirmation.getUser();
		user.setEnabled(true);
		this.userDAO.save(user);
		this.accountConfirmationDAO.delete(accountConfirmation);

		this.sendCredentials(user);
	}

	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public AccountConfirmation validate(AccountConfirmationInterface accountConfirmationInterface) throws Exception {
		Optional<AccountConfirmation> optionalAccountConfirmation = this.accountConfirmationDAO.findByCodeAndUserEmail(
				accountConfirmationInterface.getCode(), accountConfirmationInterface.getEmail());

		if (optionalAccountConfirmation.isEmpty()) {
			throw new InvalidConfirmationCodeException("The code provided is invalid for this email");
		}

		AccountConfirmation accountConfirmation = optionalAccountConfirmation.get();

		if (LocalDateTime.now().isAfter(accountConfirmation.getExpirationDate())) {
			this.accountConfirmationDAO.delete(accountConfirmation);
			throw new InvalidConfirmationCodeException("The code provided has expired");
		}

		return accountConfirmation;
	}

	private void sendCredentials(User user) {
		CredentialsDTO credentialsDTO = new CredentialsDTO(user);
		List<CredentialsDTO> credentialsList = Arrays.asList(credentialsDTO);
		this.kafkaTemplate.send(Topics.SYNC_CREDENTIALS_TOPIC, credentialsList);
	}

}
