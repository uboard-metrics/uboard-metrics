package br.com.uboard.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import br.com.uboard.core.GitlabManager;
import br.com.uboard.exception.UserAlreadyActivatedException;
import br.com.uboard.exception.UserAlreadyExistsException;
import br.com.uboard.exception.UserNotFoundException;
import br.com.uboard.model.AccountConfirmation;
import br.com.uboard.model.User;
import br.com.uboard.model.enums.AccountConfirmationTypeEnum;
import br.com.uboard.model.transport.CredentialsDTO;
import br.com.uboard.model.transport.MailDTO;
import br.com.uboard.model.transport.NewPasswordDTO;
import br.com.uboard.model.transport.UserDTO;
import br.com.uboard.repository.AccountConfirmationDAO;
import br.com.uboard.repository.UserDAO;
import br.com.uboard.utils.RandomUtils;

@Service
public class UserService {

	private static final Logger LOGGER = LoggerFactory.getLogger(UserService.class);

	private UserDAO userDAO;

	private AccountConfirmationDAO accountConfirmationDAO;

	private AccountConfirmationService accountConfirmationService;

	private MailService mailService;

	private GitlabManager gitlabManager;

	private PasswordEncoder passwordEncoder;

	public UserService(UserDAO userDAO, AccountConfirmationDAO accountConfirmationDAO,
			AccountConfirmationService accountConfirmationService, MailService mailService, GitlabManager gitlabManager,
			PasswordEncoder passwordEncoder) {
		this.userDAO = userDAO;
		this.accountConfirmationDAO = accountConfirmationDAO;
		this.accountConfirmationService = accountConfirmationService;
		this.mailService = mailService;
		this.gitlabManager = gitlabManager;
		this.passwordEncoder = passwordEncoder;
	}

	public UserDTO fetchUserFromGitlab(CredentialsDTO credentialsDTO) throws Exception {
		return this.gitlabManager.fetchUser(credentialsDTO);
	}

	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public void create(UserDTO userDTO) throws UserAlreadyExistsException {
		this.userAlreadyExists(userDTO.getEmail());

		User user = new User(userDTO);
		user.setPassword(this.passwordEncoder.encode(userDTO.getPassword()));
		this.userDAO.save(user);

		LOGGER.debug("Send activation account code to registered user...");

		String subject = "Confirmação de conta - uBoard Metrics";
		String template = "account-confirmation-code.html";
		String type = AccountConfirmationTypeEnum.ACTIVATE_USER.getName();

		this.sendConfirmAccountCode(subject, template, type, user);
	}

	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public void resendConfirmationCode(String email) throws UserNotFoundException, UserAlreadyActivatedException {
		Optional<User> optionalUser = this.userDAO.findByEmail(email);

		if (optionalUser.isEmpty()) {
			throw new UserNotFoundException("The email entered does not belong to any registered user");
		}

		User user = optionalUser.get();
		if (user.isEnabled()) {
			throw new UserAlreadyActivatedException("This user is already activated");
		}

		String type = AccountConfirmationTypeEnum.ACTIVATE_USER.getName();
		Optional<AccountConfirmation> optionalAccountConfirmation = this.accountConfirmationDAO.findByUserAndType(user,
				type);

		if (optionalAccountConfirmation.isPresent()) {
			this.accountConfirmationDAO.delete(optionalAccountConfirmation.get());
		}

		String subject = "Confirmação de conta - uBoard Metrics";
		String template = "account-confirmation-code.html";

		this.sendConfirmAccountCode(subject, template, type, user);
	}

	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public void requestResetPassword(String email) throws UserNotFoundException, UserAlreadyActivatedException {
		Optional<User> optionalUser = this.userDAO.findByEmail(email);

		if (optionalUser.isEmpty()) {
			throw new UserNotFoundException("The email entered does not belong to any registered user");
		}

		String type = AccountConfirmationTypeEnum.RESET_PASSWORD.getName();
		Optional<AccountConfirmation> optionalAccountConfirmation = this.accountConfirmationDAO
				.findByUserAndType(optionalUser.get(), type);

		if (optionalAccountConfirmation.isPresent()) {
			this.accountConfirmationDAO.delete(optionalAccountConfirmation.get());
		}

		String subject = "Recuperação de senha - uBoard Metrics";
		String template = "reset-password-confirmation-code.html";

		this.sendConfirmAccountCode(subject, template, type, optionalUser.get());
	}

	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public void resetPassword(NewPasswordDTO newPasswordDTO) throws Exception {
		AccountConfirmation accountConfirmation = this.accountConfirmationService.validate(newPasswordDTO);

		User user = accountConfirmation.getUser();
		user.setPassword(this.passwordEncoder.encode(newPasswordDTO.getPassword()));
		this.userDAO.save(user);
		this.accountConfirmationDAO.delete(accountConfirmation);
	}

	@Transactional(propagation = Propagation.REQUIRED, readOnly = true)
	public List<CredentialsDTO> fetchCredentialsToSync() {
		List<User> users = this.userDAO.findAll();
		if (users.isEmpty() || users == null) {
			return new ArrayList<>();
		}

		List<CredentialsDTO> credentialsList = new ArrayList<>();
		for (User user : users) {
			CredentialsDTO credentialsDTO = new CredentialsDTO(user);
			credentialsList.add(credentialsDTO);
		}

		return credentialsList;
	}

	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	private void sendConfirmAccountCode(String subject, String template, String type, User user) {
		MailDTO mailDTO = new MailDTO();
		mailDTO.setTo(user.getEmail());
		mailDTO.setSubject(subject);
		mailDTO.setTemplate(template);

		String code = RandomUtils.getInstance().generateRandomCode();
		AccountConfirmation accountConfirmation = new AccountConfirmation();
		accountConfirmation.setCode(code);
		accountConfirmation.setUser(user);
		accountConfirmation.setType(type);
		this.accountConfirmationDAO.save(accountConfirmation);

		Map<String, Object> properties = new HashMap<>();
		properties.put("username", user.getName());
		properties.put("confirmationCode", code);
		mailDTO.setProperties(properties);
		this.mailService.send(mailDTO);
	}

	@Transactional(propagation = Propagation.REQUIRED, readOnly = true)
	private void userAlreadyExists(String email) throws UserAlreadyExistsException {
		Optional<User> optionalUser = this.userDAO.findByEmail(email);

		if (optionalUser.isPresent()) {
			throw new UserAlreadyExistsException("User with e-mail: " + email + " already exists");
		}
	}
}
