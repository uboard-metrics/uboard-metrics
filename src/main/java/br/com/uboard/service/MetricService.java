package br.com.uboard.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import br.com.uboard.model.Developer;
import br.com.uboard.model.Issue;
import br.com.uboard.model.Metric;
import br.com.uboard.model.User;
import br.com.uboard.model.transport.DeveloperDTO;
import br.com.uboard.model.transport.IssueDTO;
import br.com.uboard.model.transport.MetricDTO;
import br.com.uboard.model.transport.UserDTO;
import br.com.uboard.repository.DeveloperDAO;
import br.com.uboard.repository.IssueDAO;
import br.com.uboard.repository.MetricDAO;
import br.com.uboard.repository.UserDAO;

@Service
public class MetricService {

	private static final Logger LOGGER = LoggerFactory.getLogger(MetricService.class);

	private MetricDAO metricDAO;

	private DeveloperDAO developerDAO;

	private IssueDAO issueDAO;

	private UserDAO userDAO;

	public MetricService(MetricDAO metricDAO, DeveloperDAO developerDAO, IssueDAO issueDAO, UserDAO userDAO) {
		this.metricDAO = metricDAO;
		this.developerDAO = developerDAO;
		this.issueDAO = issueDAO;
		this.userDAO = userDAO;
	}

	@Transactional(propagation = Propagation.REQUIRED, readOnly = true)
	public List<MetricDTO> listByUser(UserDTO userInSession, String milestone) throws Exception {
		Optional<User> optionalUser = this.userDAO.findByUboardIdentifier(userInSession.getUboardIdentifier());
		if (optionalUser.isEmpty()) {
			throw new Exception("User is not found");
		}

		List<Metric> metricsByUser = this.metricDAO.findByUserAndMilestoneContains(optionalUser.get(), milestone);
		if (metricsByUser == null || metricsByUser.isEmpty()) {
			return new ArrayList<>();
		}

		return metricsByUser.stream().map(MetricDTO::new).collect(Collectors.toList());
	}

	@Transactional(propagation = Propagation.REQUIRED, readOnly = true)
	public List<DeveloperDTO> listDevelopersByMetric(UserDTO userInSession, String name, String metricUUID)
			throws Exception {
		Optional<User> optionalUser = this.userDAO.findByUboardIdentifier(userInSession.getUboardIdentifier());
		if (optionalUser.isEmpty()) {
			throw new Exception("User is not found");
		}

		User user = optionalUser.get();
		Optional<Metric> optionalMetric = this.metricDAO.findByUserAndUboardIdentifier(user, metricUUID);
		if (optionalMetric.isEmpty()) {
			throw new Exception("Metric not found");
		}

		Metric metric = optionalMetric.get();
		List<Developer> developers = this.developerDAO.findByMetricAndNameContains(metric, name);
		if (developers == null || developers.isEmpty()) {
			return new ArrayList<>();
		}

		return developers.stream().map(DeveloperDTO::new).collect(Collectors.toList());
	}

	@Transactional(propagation = Propagation.REQUIRED, readOnly = true)
	public List<IssueDTO> listIssuesByMetric(UserDTO userInSession, String title, String metricUUID) throws Exception {
		Optional<User> optionalUser = this.userDAO.findByUboardIdentifier(userInSession.getUboardIdentifier());
		if (optionalUser.isEmpty()) {
			throw new Exception("User is not found");
		}

		User user = optionalUser.get();
		Optional<Metric> optionalMetric = this.metricDAO.findByUserAndUboardIdentifier(user, metricUUID);
		if (optionalMetric.isEmpty()) {
			throw new Exception("Metric not found");
		}

		Metric metric = optionalMetric.get();
		List<Issue> issues = this.issueDAO.findByMetricAndTitleContains(metric, title);
		if (issues == null || issues.isEmpty()) {
			return new ArrayList<>();
		}

		return issues.stream().map(IssueDTO::new).collect(Collectors.toList());
	}

	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public void persist(List<MetricDTO> metricsList) throws Exception {
		try {
			if (metricsList != null && !metricsList.isEmpty()) {
				List<Metric> metrics = metricsList.stream().map(Metric::new).collect(Collectors.toList());
				for (Metric metric : metrics) {
					Optional<User> optionalUser = this.userDAO
							.findByUboardIdentifier(metric.getUser().getUboardIdentifier());
					if (optionalUser.isPresent()) {
						User user = optionalUser.get();
						Optional<Metric> optionalMetric = this.metricDAO.findByMilestoneAndUser(metric.getMilestone(),
								user);

						if (optionalMetric.isEmpty()) {
							metric.setUser(user);
							this.metricDAO.save(metric);
						} else {
							Metric currentMetric = optionalMetric.get();
							currentMetric = this.buildMetricUpdate(currentMetric, metric);
							this.metricDAO.save(currentMetric);
						}
					}
				}
			}
		} catch (Exception e) {
			LOGGER.error(e.getMessage(), e);
			throw new Exception(e.getMessage());
		}
	}

	private Metric buildMetricUpdate(Metric currentMetric, Metric newMetric) {
		currentMetric.setMilestone(newMetric.getMilestone());
		currentMetric.setStartDate(newMetric.getStartDate());
		currentMetric.setDueDate(newMetric.getDueDate() != null ? newMetric.getDueDate() : null);
		currentMetric.setTotalIssues(newMetric.getTotalIssues());
		currentMetric.setOpenIssuesCount(newMetric.getOpenIssuesCount());
		currentMetric.setClosedIssuesCount(newMetric.getClosedIssuesCount());
		currentMetric.setReopenedIssuesCount(newMetric.getReopenedIssuesCount());
		currentMetric.setConclusionPercent(newMetric.getConclusionPercent());
		currentMetric.setDeficit(newMetric.getDeficit());

		if (newMetric.getDevelopers() != null && !newMetric.getDevelopers().isEmpty()) {
			for (Developer newDeveloper : newMetric.getDevelopers()) {
				for (Developer currentDeveloper : currentMetric.getDevelopers()) {
					if (currentDeveloper.getName().equals(newDeveloper.getName())) {
						currentDeveloper.setName(newDeveloper.getName());
						currentDeveloper.setCompletedIssues(newDeveloper.getCompletedIssues());
						currentDeveloper.setConclusionPercent(newDeveloper.getConclusionPercent());
						currentDeveloper.setIdle(newDeveloper.isIdle());
					}
				}
			}
		}

		if (newMetric.getIssues() != null && !newMetric.getIssues().isEmpty()) {
			for (Issue newIssue : newMetric.getIssues()) {
				for (Issue currentIssue : currentMetric.getIssues()) {
					if (currentIssue.getTitle().equals(newIssue.getTitle())) {
						currentIssue.setTitle(newIssue.getTitle());
						currentIssue.setCreatedAt(newIssue.getCreatedAt());
						currentIssue.setClosedAt(newIssue.getClosedAt() != null ? newIssue.getClosedAt() : null);
						currentIssue.setState(newIssue.getState());
						currentIssue.setHasAssignees(newIssue.isHasAssignees());
					}
				}
			}
		}

		return currentMetric;
	}
}
