package br.com.uboard.service;

import java.io.Serializable;

import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

import br.com.uboard.config.kafka.producer.Topics;
import br.com.uboard.model.transport.MailDTO;

@Service
public class MailService {

	private KafkaTemplate<String, Serializable> kafkaTemplate;

	public MailService(KafkaTemplate<String, Serializable> kafkaTemplate) {
		this.kafkaTemplate = kafkaTemplate;
	}

	public void send(MailDTO mailDTO) {
		kafkaTemplate.send(Topics.SEND_MAIL_TOPIC, mailDTO);
	}

}
