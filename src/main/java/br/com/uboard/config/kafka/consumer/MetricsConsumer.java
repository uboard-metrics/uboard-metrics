package br.com.uboard.config.kafka.consumer;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Component;

import br.com.uboard.model.transport.MetricDTO;
import br.com.uboard.service.MetricService;

@Component
public class MetricsConsumer {

	private static final Logger LOGGER = LoggerFactory.getLogger(MetricsConsumer.class);
	private static final String SEND_METRICS_TOPIC = "send-metrics-topic";

	private MetricService metricService;

	public MetricsConsumer(MetricService metricService) {
		this.metricService = metricService;
	}

	@KafkaListener(groupId = "receive-metrics-group", topics = SEND_METRICS_TOPIC, containerFactory = "metricsContainerFactory")
	public void receive(@Payload List<MetricDTO> metricsList) throws Exception {
		try {
			LOGGER.debug("Getting {} metrics to persist", metricsList.size());
			if (metricsList.size() > 0) {
				this.metricService.persist(metricsList);
			}
		} catch (Exception e) {
			LOGGER.error(e.getMessage(), e);
		}
	}
}
