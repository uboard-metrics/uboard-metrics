package br.com.uboard.config.kafka.producer;

public class Topics {

	public static final String SEND_MAIL_TOPIC = "send-mail-topic";
	public static final String SYNC_CREDENTIALS_TOPIC = "sync-credentials-topic";

}
