package br.com.uboard.controller.schedulers;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import br.com.uboard.config.kafka.producer.Topics;
import br.com.uboard.model.transport.CredentialsDTO;
import br.com.uboard.service.UserService;

@Component
@EnableAsync
public class SyncCredentialsScheduler {

	private static final Logger LOGGER = LoggerFactory.getLogger(SyncCredentialsScheduler.class);

	private KafkaTemplate<String, List<CredentialsDTO>> kafkaTemplate;

	private UserService userService;

	public SyncCredentialsScheduler(KafkaTemplate<String, List<CredentialsDTO>> kafkaTemplate,
			UserService userService) {
		this.kafkaTemplate = kafkaTemplate;
		this.userService = userService;
	}

	@Async
	@Scheduled(fixedDelay = 180000)
	public void sendCredentials() {
		LOGGER.info("Fetching and sending credentials...");
		List<CredentialsDTO> credentialsList = this.userService.fetchCredentialsToSync();
		if (!credentialsList.isEmpty()) {
			this.kafkaTemplate.send(Topics.SYNC_CREDENTIALS_TOPIC, credentialsList);
		}
	}

}
