package br.com.uboard.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import br.com.uboard.model.transport.AccountConfirmationDTO;
import br.com.uboard.model.transport.CredentialsDTO;
import br.com.uboard.model.transport.NewPasswordDTO;
import br.com.uboard.model.transport.UserDTO;
import br.com.uboard.service.AccountConfirmationService;
import br.com.uboard.service.UserService;
import br.com.uboard.service.UserSessionService;
import jakarta.validation.Valid;

@RestController
@RequestMapping("/user")
public class UserController {

	private static final Logger LOGGER = LoggerFactory.getLogger(UserController.class);

	private UserService userService;

	private AccountConfirmationService accountConfirmationService;

	private UserSessionService userSessionService;

	public UserController(UserService userService, AccountConfirmationService accountConfirmationService,
			UserSessionService userSessionService) {
		this.userService = userService;
		this.accountConfirmationService = accountConfirmationService;
		this.userSessionService = userSessionService;
	}

	@PostMapping("/sync")
	public ResponseEntity<UserDTO> fetchUserFromGitlab(@Valid @RequestBody CredentialsDTO credentialsDTO)
			throws Exception {
		try {
			UserDTO response = this.userService.fetchUserFromGitlab(credentialsDTO);
			return ResponseEntity.ok(response);
		} catch (Exception e) {
			LOGGER.error(e.getMessage(), e);
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage(), e);
		}
	}

	@PostMapping
	public ResponseEntity<Void> create(@Valid @RequestBody UserDTO userDTO) throws Exception {
		try {
			this.userService.create(userDTO);
			return ResponseEntity.ok().build();
		} catch (Exception e) {
			LOGGER.error(e.getMessage(), e);
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage(), e);
		}
	}

	@PostMapping("/activation")
	public ResponseEntity<Void> activateAccount(@Valid @RequestBody AccountConfirmationDTO accountConfirmationDTO)
			throws Exception {
		try {
			this.accountConfirmationService.activate(accountConfirmationDTO);
			return ResponseEntity.ok().build();
		} catch (Exception e) {
			LOGGER.error(e.getMessage(), e);
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage(), e);
		}
	}

	@PostMapping("/activation/resend")
	public ResponseEntity<Void> resendActivateAccount(@RequestBody String email) throws Exception {
		try {
			this.userService.resendConfirmationCode(email);
			return ResponseEntity.ok().build();
		} catch (Exception e) {
			LOGGER.error(e.getMessage(), e);
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage(), e);
		}
	}

	@PostMapping("/reset-password")
	public ResponseEntity<Void> requestResetPassowrd(@RequestBody String email) throws Exception {
		try {
			this.userService.requestResetPassword(email);
			return ResponseEntity.ok().build();
		} catch (Exception e) {
			LOGGER.error(e.getMessage(), e);
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage(), e);
		}
	}

	@PutMapping("/reset-password")
	public ResponseEntity<Void> resetPassword(@Valid @RequestBody NewPasswordDTO newPasswordDTO) throws Exception {
		try {
			this.userService.resetPassword(newPasswordDTO);
			return ResponseEntity.ok().build();
		} catch (Exception e) {
			LOGGER.error(e.getMessage(), e);
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage(), e);
		}
	}

	@GetMapping("/teste")
	public ResponseEntity<Void> teste() throws Exception {
		try {
			UserDTO userInSession = this.userSessionService.getUserInSession();
			LOGGER.info(userInSession.getEmail());
			return ResponseEntity.ok().build();
		} catch (Exception e) {
			LOGGER.error(e.getMessage(), e);
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage(), e);
		}
	}
}
