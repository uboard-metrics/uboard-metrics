package br.com.uboard.controller;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import br.com.uboard.model.transport.DeveloperDTO;
import br.com.uboard.model.transport.IssueDTO;
import br.com.uboard.model.transport.MetricDTO;
import br.com.uboard.model.transport.UserDTO;
import br.com.uboard.service.MetricService;
import br.com.uboard.service.UserSessionService;

@RestController
@RequestMapping("/metric")
public class MetricController {

	private static final Logger LOGGER = LoggerFactory.getLogger(MetricController.class);

	private MetricService metricService;
	private UserSessionService userSessionService;

	public MetricController(MetricService metricService, UserSessionService userSessionService) {
		this.metricService = metricService;
		this.userSessionService = userSessionService;
	}

	@GetMapping
	public ResponseEntity<List<MetricDTO>> listByUser(@RequestParam("milestone") String milestone) throws Exception {
		try {
			UserDTO userInSession = this.userSessionService.getUserInSession();
			List<MetricDTO> response = this.metricService.listByUser(userInSession, milestone);
			return ResponseEntity.ok(response);
		} catch (Exception e) {
			LOGGER.error(e.getMessage(), e);
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage(), e);
		}
	}

	@GetMapping("/{metricUUID}/developer")
	public ResponseEntity<List<DeveloperDTO>> listDevelopersByMetric(@PathVariable("metricUUID") String metricUUID,
			@RequestParam("name") String name) throws Exception {
		try {
			UserDTO userInSession = this.userSessionService.getUserInSession();
			List<DeveloperDTO> response = this.metricService.listDevelopersByMetric(userInSession, name, metricUUID);
			return ResponseEntity.ok(response);
		} catch (Exception e) {
			LOGGER.error(e.getMessage(), e);
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage(), e);
		}
	}
	
	@GetMapping("/{metricUUID}/issue")
	public ResponseEntity<List<IssueDTO>> listIssuesByMetric(@PathVariable("metricUUID") String metricUUID,
			@RequestParam("title") String title) throws Exception {
		try {
			UserDTO userInSession = this.userSessionService.getUserInSession();
			List<IssueDTO> response = this.metricService.listIssuesByMetric(userInSession, title, metricUUID);
			return ResponseEntity.ok(response);
		} catch (Exception e) {
			LOGGER.error(e.getMessage(), e);
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage(), e);
		}
	}

}
