package br.com.uboard;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@EnableScheduling
public class UboardMetricsApplication {

	public static void main(String[] args) {
		SpringApplication.run(UboardMetricsApplication.class, args);
	}

}
