package br.com.uboard.core;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

import br.com.uboard.config.web.WebClientRest;
import br.com.uboard.model.transport.CredentialsDTO;
import br.com.uboard.model.transport.UserDTO;

@Component
public class GitlabManager {

	@Value("${gitlab-provider.address}")
	private String providerAddress;

	private WebClientRest webClientRest;

	public GitlabManager() {
		this.webClientRest = new WebClientRest();
	}

	public UserDTO fetchUser(CredentialsDTO credentialsDTO) throws Exception {
		String uri = new StringBuilder().append(this.providerAddress).append("/provider/user").toString();
		ResponseEntity<UserDTO> response = this.webClientRest.post(uri, UserDTO.class, credentialsDTO);

		if (response.getStatusCode() != HttpStatus.OK) {
			throw new Exception("Error on retrieve user from Gitlab");
		}

		return response.getBody();
	}
}
